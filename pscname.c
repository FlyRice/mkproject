#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/*
 * Short for Project Source Code Name; used by mkproject to generate the name
 * of the actual source code file to be placed into the new project directory.
 * File naming depends on the conventions of the language specified (fucking
 * java).
 * 
 * As per mkproject's inception, this program is to be passed 2 args: 
 * filename language
 * 
 * BUGGY ASSUMPTION: most project names won't be longer than 2 words.
 * 
 */

// returns true if the char is an upper case letter
bool isup(char c){
	return (c <= 'Z') && (c >= 'A');
 }

// returns whatever word passed to it in titlecase.
char* titlecase(char* str){
	str[0] = (isup(str[0])) ? str[0] : str[0]-32;
	int i;
	for(i=1; i < strlen(str); i++){
		if(isup(str[i]))
			str[i] += 32;
	}
	return str;
 }

int main(int argc, char* argv[]){
	// if it's java, titlecase & remove hyphens
	if(strcmp(argv[2], "java")==0){
		// split on hyphens, title, glue, return
		char* pch;
		char name[50];

		if(strchr(argv[1], '-') == NULL){
			printf("%s", titlecase(argv[1]));
		}else{
			pch = strtok(argv[1], "-");	
			strcpy(name, titlecase(pch));
			pch = strtok(NULL, "-");
			strcat(name, titlecase(pch));
			printf("%s", name);
		}
	}// python doesn't really have a set convention, it being an inbred child of C++ and Java
	else if(strcmp(argv[2], "python")==0){
		printf("%s", argv[1]);
	}// else lowercase everything
	else{
		int i;
		for(i=0; i < strlen(argv[1]); i++)
			if(isup(argv[1][i]))
				argv[1][i] += 32;

		printf("%s", argv[1]);
	 }
	return 0;
 }
