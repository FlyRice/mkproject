#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Replaces spaces with hyphens for project names.
 * 
 */

int main(int argc, char* argv[]){
	int i;

	for(i=1; i<argc; i++){
		if(i == argc-1)
			printf("%s", argv[i]);
		else
			printf("%s%c", argv[i], '-');
	}

	return 0;
}
