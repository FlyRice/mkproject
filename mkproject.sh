#!/usr/bin/env bash
################################################################################
 #                                  mkproject                                   #
 #                                                                              #
 # Simple bash/C scripts to auto-generate a new project directory with a        #
 # template source code file copied over and some standard info/config files.   #
 #                                                                              #
 ################################################################################
 #                              Change History                                  #
 # 16/12/2020  v0.1    Original code.                                           #
 # 19/12/2020  v1.0    Pushed to git and tackled some todo items.               #
 # 29/12/2020  v1.1    Reimplementation using itself.                           #
 #                     -Expanded language specification.                        #
 #                     -Added option to save to clipboard (requires xclip)      #
 #                     -Enabled command-line args functionality                 #
 #                     -Refined paths and variables to be more universial/      #
 #                      customizable.                                           #
 #                                                                              #
 ################################################################################
 #                                                                              #
 #  Copyright (C) 2007, 2020 FlyRice                                            #
 #  Schyler.NW@gmail.com                                                        #
 #                                                                              #
 #  This program is free software; you can redistribute it and/or modify it     #
 #  under the terms of the GNU General Public License as published by the Free  #
 #  Software Foundation; either version 2 of the License, or (at your option)   #
 #  any later version.                                                          #
 #                                                                              #
 #  This program is distributed in the hope that it will be useful, but WITHOUT #
 #  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
 #  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for   #
 #  more details.                                                               #
 #                                                                              #
 #  You should have received a copy of the GNU General Public License along     #
 #  with this program; if not, write to the Free Software Foundation, Inc.,     #
 #  59 Temple Place, Suite 330, Boston, MA  02111-1307  USA. Alternatively      #
 #  (because I'm new to professional documentation when making projects), let   #
 #  me know at the email given above.                                           #
 #                                                                              #
  ################################################################################

# get inputs (if no args)
name=""
mode=""
lang=""

# parse args (if any (while any, w/e))
while [[ $# -gt 0 ]]; do
	key="$1"
	case $key in
		-h|--help)
			cat $HOME/Documents/dev/mkproject/help.txt
			exit 0;;
		-v|--version)
			head -n1 $HOME/Documents/dev/mkproject/help.txt
			exit 0;;
		-n|--name)
			name=$2
			shift	# past argument
			shift;;
		-m|--mode)
			mode=$2
			shift
			shift;;
		-l|--lang)
			lang=$2
			shift
			shift;;
		*)
			echo "Error: Unknown argument: $1"
			exit 1;;
	esac
done

# get user input for values not in args
# there's probably a better way to do this but I can't be asked right now
if [[ -z $name ]]; then
	printf "name: "
	read name
fi
if [[ -z $mode ]]; then
	printf "mode: "
	read mode
fi
if [[ -z $lang ]]; then
	printf "language: "
	read lang
fi
echo ""

# lowercase the mode just to make it easier to work with whenever I decide to start working with it
mode="$($HOME/bin/pscname $mode c)"

### program body
# work out the extension & standardize languages for helper programs/scripts
case $lang in
	"bash")
		ext="sh"
		lang="bash";;
	"c"|"C")
		ext="c"
		lang="c";;
	"cpp"|"Cpp"|"CPP"|"c++"|"C++")
		ext="cpp"
		lang="cpp";;
	"java"|"Java")
		ext="java"
		lang="java";;
	"py"|"python"|"python2"|"python3")
		ext="py"
		lang="python";;
esac

echo "$lang extension: $ext"

### paths and variables; customize as you please, be wary of editing anything outside this section
# just where I put all the "helper" files for my custom scripts/programs; change these to fit your directory structure
srcpath="$HOME/src"
tpath="$srcpath/mkproject/templates"
templ="$tpath/cli-template.$ext"

# remove the spaces in whatever name inputted
name="$($HOME/bin/fixname $name)"
echo "template file: $templ"

# project path
ppath="$HOME/Documents/dev/"

### end of safe editing section; back to the doing of things
# go to & create the project directory (maybe do some other stuff here, like open sublime)
cd $ppath
mkdir "$name"
cd $name
ppath="$ppath/$name"
printf "project path: $ppath\n"
printf "Would you like to copy the project path to the clipboard?(y/n): "
read ans

if [[ $ans -eq "y" ]]; then
	pwd | xclip -selection clipboard
fi

# get appropriate name for the source code file as per its language specification
scname="$($HOME/bin/pscname $name $lang).$ext"
echo "source code file: $scname"

### pretty safe to edit this as well; comment/uncomment as you please, just be sure to edit the files in your templates path as necessary
touch $scname README.md help.txt outline.txt todo.txt config
cp $templ $scname
cp $tpath/README.md .
cp $tpath/help.txt .
#cp $tpath/outline.txt .
cp $tpath/todo.txt .
echo "created standard project files"

# makes source code executable if script files (shell/python); templates probably error-prone
if [[ $ext -eq "sh" || $ext -eq "py" ]]; then
	chmod +x $scname
	echo "Gave $scname executable rights"
fi
