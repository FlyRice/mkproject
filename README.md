simple bash/C script(s) to auto-generate a new project directory with a template source code file copied over and some standard info/config files.

The way my preferred directory structure for programming/scripting projects works is that I have a development (dev) folder in my ~/Documents/, a ~/bin folder in my path with executables and symlinks to executables with easier names (the extensions chopped off) pointing to a ~/src directory where all my program-helper files exist. My ~/src directory is full of symlinks to finished projects in my ~/Documents/dev directory because that may as well be the case.

Although this follows some semblance of a convention somewhere, I realize it's not a fully traditional Linux development directory structure where I'd perhaps put my finished projects in /opt/ or /etc/. Rather than doing loads of research where to properly place everything, I find that this is enough organization for now, and at least for starting off in Linux development.

Thought-tracking EDIT: I'm now aware of the XDG standards/whatever, and now know the purpose of .config, .cache, and .local/share.

Dependencies: xclip shellcheck
