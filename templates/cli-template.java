import java.util.Scanner;
import java.util.Formatter;
import java.io.File;

/********************************************************************************
 *                               programName                                    *
 *                                                                              *
 * Use this template as the beginning of a new program. Place a short           *
 * description of the script here.                                              *
 *                                                                              *
 ********************************************************************************
 *                              Change History                                  *
 * 11/11/2019  David Both    Original code. This is a template for creating     *
 *                           new Bash shell scripts.                            *
 *                           Add new history entries as needed.                 *
 *                                                                              *
 ********************************************************************************
 *                                     TODO                                     *
 * - Finish removing remnants of my ongoing "Todo List"/Scheduling program from *
 *   here.                                                                      *
 *                                                                              *
 ********************************************************************************
 *  Copyright (C) 2007, 2020 FlyRice                                            *
 *  Schyler.NW@gmail.com                                                        *
 *                                                                              *
 *  This program is free software; you can redistribute it and/or modify        *
 *  it under the terms of the GNU General Public License as published by        *
 *  the Free Software Foundation; either version 2 of the License, or           *
 *  (at your option) any later version.                                         *
 *                                                                              *
 *  This program is distributed in the hope that it will be useful,             *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 *  GNU General Public License for more details.                                *
 *                                                                              *
 *  You should have received a copy of the GNU General Public License           *
 *  along with this program; if not, write to the Free Software                 *
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA   *
 *  Alternatively (because I'm new to professional documentation when making    *
 *  projects), let me know at the email given above.                            *
 *                                                                              *
 ********************************************************************************/

public class JSchedule{
	private static Scanner fin;
	private static Formatter fout;
	
	// program body methods

	/** Determines whether a string can be converted to an integer
	 * 
	 * @arg str	-- a string that maybe represents a number
	 * @return false if the string doesn't resemble a number
	 */
	private static boolean isNum(String str){
		try{
			int num = Integer.parseInt(str);
		}catch(NumberFormatException | NullPointerException nfe){
			return false;
		}
		return true;
	 }

	// main method / program body
	public static void main(String[]args){
		// declare some vars
		fin = new Scanner(System.in);

		if(args.size() == 1){
			// default behavior (prompt user input)
			System.out.print("file name: ");
			String text = fin.nextLine();
			System.out.print("password: ");
			String pass = fin.nextLine();

			// OR print error message
			//return errmsg(args[0]);
		}else if(args[0].equals("--help") || args[0].equals("-h")){
			help();
		}else if(args[0].equals("--version") || args[0].equals("-v")){
			version();
		}else{
			if(parseArgs(args)==1)	return 1;
			// TODO run program
		}
	 }

	// standard methods section
	private static void help(){
		// TODO
	 }
	private static void version(){
		// TODO
	 }
	private static void parseArgs(String[]args){

		if(args[0].equals("display") && args[1].equals("todo"))
			todolist.displayTodo();
		else if(args[0].equals("display") && isNum(args[1]))
			todolist.displayTask(Integer.parseInt(args[1]));
		else
			for(String str : args)
				System.out.println(str);

		// TODO 
	 }
	private static int errmsg(String err){
		System.out.println("error: unknown/invalid command argument: " + err);
		// maybe I should define some Exceptions
		//[obj].printStackTrace();
	 }

	// file-centric methods section
	private static void openFile(String name, String mode){
		file = new File(name);

		if(mode.equals("r")){
			try{
				fin = new Scanner(file);
			}catch(Exception e){
				e.printStackTrace();
			}
		}else if(mode.equals("w")){
			try{
				fout = new Formatter(file);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	 }
	private static void readFile(){
		while(fin.hasNext()){
			String str = fin.nextLine();
			String[]strs = str.split(":");		// strs.length should be 3
			int id = Integer.parseInt(strs[0]);
		}
	 }
	private static void saveFile(){
		// delete current file
		file.delete();

		// TODO define a format for writing to a file
		
	 }
	private static void closeFile(){
		fin.close();
		fin = new Scanner(System.in);
	 }
}
