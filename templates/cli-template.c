#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

/********************************************************************************
 *                               programName                                    *
 *                                                                              *
 * Use this template as the beginning of a new program. Place a short           *
 * description of the script here.                                              *
 *                                                                              *
 ********************************************************************************
 *                              Change History                                  *
 * 11/11/2019  David Both    Original code. This is a template for creating     *
 *                           new Bash shell scripts.                            *
 *                           Add new history entries as needed.                 *
 *                                                                              *
 *                                                                              *
 ********************************************************************************
 *  Copyright (C) 2007, 2020 FlyRice                                            *
 *  Schyler.NW@gmail.com                                                        *
 *                                                                              *
 *  This program is free software; you can redistribute it and/or modify        *
 *  it under the terms of the GNU General Public License as published by        *
 *  the Free Software Foundation; either version 2 of the License, or           *
 *  (at your option) any later version.                                         *
 *                                                                              *
 *  This program is distributed in the hope that it will be useful,             *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 *  GNU General Public License for more details.                                *
 *                                                                              *
 *  You should have received a copy of the GNU General Public License           *
 *  along with this program; if not, write to the Free Software                 *
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA   *
 *  Alternatively (because I'm new to professional documentation when making    *
 *  projects), let me know at the email given above.                            *
 *                                                                              *
 ********************************************************************************/

// standard functions section
void help(){
	// TODO read from necessary files
 }
void version(){
	// TODO read from necessary files
 }
int parseArgs(char*){
	// parse shorthand -[flags] [value]

	// parse longhand --[flags]=[value]

	// error case
	printf("Error reading arguments.\n");
	printf("syntax: crypt [file name] [password]\n");
	printf("note: consider improving and standardizing this message across languages\n");
	return 1;
 }
int errmsg(char* err){
	printf("error: unknown/invalid command argument: ");
	printf("%s\n", err);
 }

// program body ADT objects (unions, typedefs, etc.)

// program body functions (prototypes)
//void function1(arg-type list);
//void function2(arg-type list);

/** Determines whether a string can be converted to an integer
 * 
 * @arg str	-- a string that maybe represents a number
 * @return false if the string doesn't resemble a number
 */
int isNum(char* str){
	return atoi(str);	// ERROR: atoi() is fucking dead and I have to find a workaround for this fucking bullshit!
 }

int main(int argc, char* argv[]){

	if(argc == 1){
		// default behavior (prompt user input)
		printf("file name: ");
		fgets(text, 1000, stdin);
		printf("password: ");
		fgets(pass, 1000, stdin);

		// OR print error message
		//return errmsg(argv[1]);
	}else if(strcmp(argv[1], "--help")==0 || strcmp(argv[1], "-h")==0){
		help();
	}else if(strcmp(argv[1], "--version")==0 || strcmp(argv[1], "-v")==0){
		version();
	}else{
		if(parseArgs()==1)	return 1;
		// TODO run program
	}

	return 0;
 }
