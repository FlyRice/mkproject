#!/bin/bash
################################################################################
 #                              scriptTemplate                                  #
 #                                                                              #
 # Use this template as the beginning of a new program. Place a short           #
 # description of the script here.                                              #
 #                                                                              #
 ################################################################################
 #                              Change History                                  #
 # 11/11/2019  v0.1    Original code. This is a template for creating new Bash  #
 #                     shell scripts. Add new history entries as needed.        #
 #                                                                              #
 ################################################################################
 #                                                                              #
 #  Copyright (C) 2007, 2020 FlyRice                                            #
 #  Schyler.NW@gmail.com                                                        #
 #                                                                              #
 #  This program is free software; you can redistribute it and/or modify it     #
 #  under the terms of the GNU General Public License as published by the Free  #
 #  Software Foundation; either version 2 of the License, or (at your option)   #
 #  any later version.                                                          #
 #                                                                              #
 #  This program is distributed in the hope that it will be useful, but WITHOUT #
 #  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or       #
 #  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for   #
 #  more details.                                                               #
 #                                                                              #
 #  You should have received a copy of the GNU General Public License along     #
 #  with this program; if not, write to the Free Software Foundation, Inc.,     #
 #  59 Temple Place, Suite 330, Boston, MA  02111-1307  USA. Alternatively      #
 #  (because I'm new to professional documentation when making projects), let   #
 #  me know at the email given above.                                           #
 #                                                                              #
  ################################################################################

### essential vars section
# an example var to start you off in collecting the necessary information your program will use
mode=""

### functions section
# parse args function 
while [[ $# -gt 0 ]]; do
	key="$1"
	case $key in
		-h|--help)
			cat help.txt
			exit 0
			;;
		-v|--version)
			head -n1 help.txt
			exit 0
			;;
		-m|--mode)
			mode=$2
			shift	# past [2] arguments
			shift
			;;
		*)
			echo "Error: Unknown argument: $1"
			exit 1
			;;
	esac
done

# collect data not provided by args
if [[ -z $mode ]]; then
	printf "mode: "
	read mode
fi

### program body
echo "hello mode: $mode"
