#!/bin/python

################################################################################
 #                              scriptTemplate                                  #
 #                                                                              #
 # Use this template as the beginning of a new program. Place a short           #
 # description of the script here.                                              #
 #                                                                              #
 ################################################################################
 #                              Change History                                  #
 # 11/11/2019  David Both    Original code. This is a template for creating     #
 #                           new Bash shell scripts.                            #
 #                           Add new history entries as needed.                 #
 #                                                                              #
 #                                                                              #
 ################################################################################
 #                                                                              #
 #  Copyright (C) 2007, 2020 FlyRice                                            #
 #  Schyler.NW@gmail.com                                                        #
 #                                                                              #
 #  This program is free software; you can redistribute it and/or modify        #
 #  it under the terms of the GNU General Public License as published by        #
 #  the Free Software Foundation; either version 2 of the License, or           #
 #  (at your option) any later version.                                         #
 #                                                                              #
 #  This program is distributed in the hope that it will be useful,             #
 #  but WITHOUT ANY WARRANTY; without even the implied warranty of              #
 #  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
 #  GNU General Public License for more details.                                #
 #                                                                              #
 #  You should have received a copy of the GNU General Public License           #
 #  along with this program; if not, write to the Free Software                 #
 #  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA   #
 #  Alternatively (because I'm new to professional documentation when making    #
 #  projects), let me know at the email given above.                            #
 #                                                                              #
  ################################################################################

import sys
import os

# defined flags
version = 1.0
origf = None

### standard functions section
def help():
	vers()
	print("-------------------------------------------------------------------------")
	origf = open("help", "r")
	print(origf.read())
	origf.close()
def vers():		# short for version
	print("rmln.py v{}".format(version))
	pass
def parseArgs(args):
	# parse shorthand -[flags] [value]

	# parse longhand --[flags]=[value]

	# error case

	pass
def errmsg(err):
	print("error: unknown/invalid command argument:")
	print(err)

### program body functions
#def function1(arg list):\n\t// TODO
#def function2(arg list):\n\t// TODO

# Determines whether a string can be converted to an integer
 # 
 # arg str	-- a string that maybe represents a number
 # return false if the string doesn't resemble a number
bool isNum(char* str):
	try:			# review try/catch in python
		int(str)
		return True
	except:
		return False

def main():
	if len(sys.argv) == 1:
		vers()
		print("type --help or -h to see how to use this command")
		# or define a default function without arguments
	else:
		args = sys.argv[1:]
		if "--help" in args or "-h" in args:
			help()
		elif "--version" in args or "-v" in args:
			vers()
		else:
			args = parseArgs(args)
			
main()
