Just a simple script to remove specific lines from a file. Meant to work with the nl command (ignores empty lines)

Realized too late into this implementation that I could just use awk/sed/grep, pipes and other bash shortcuts to do the same thing. I'll use this as an example program from now on.
